﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
	public Vector3 perTick;
	public Rigidbody myBody;
	/*/
	 * A geostationary satellite completes one orbit per day above the equator, or 360 degrees per 24 hours, and has angular velocity ω=360/24=15 degrees per hour, or 2π/24≈0.26 radians per hour.
	 * If angle is measured in radians, the linear velocity is the radius times the angular velocity, v=r*ω.
	 * With orbital radius 42,000 km from the earth's center, the satellite's speed through space is thus v = 42,000 × 0.26 ≈ 11,000 km/hr.
	/*/
	public bool affectedBySettings = true;
	/// <summary>
	/// Inverts the settings.
	/// </summary>
	public bool inverseSettings = false;

	void Start()
	{
		if(affectedBySettings && GameManager.instance.enableRotation == false)
		{
			this.enabled = false;
		}
		else if(inverseSettings)
		{
			this.enabled = !GameManager.instance.enableRotation;
		}
	}

	void FixedUpdate()
	{
		if(myBody == null)
		{
			transform.Rotate(perTick * Time.fixedDeltaTime, Space.Self);
		}
		else
		{
			Quaternion q = transform.rotation;
			q.eulerAngles = q.eulerAngles + (perTick*Time.deltaTime);
			myBody.MoveRotation(q);
		}
	}
}