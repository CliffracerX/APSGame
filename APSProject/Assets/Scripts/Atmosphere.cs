﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atmosphere : MonoBehaviour
{
	/// <summary>
	/// Some atmospheres make it easier on your life support. 
	/// 0.0f to 1.0f, multiplied against your current usage variable.
	/// E.g, in a one-layer atmosphere with 0.9f easing...
	/// ...your life support will use 10% less.
	/// If the same atmosphere is 3-layer, it accumulates.
	/// Life support will use 27.1% less.
	/// </summary>
	public float lifeSupportEasing = 1f;
	/// <summary>
	/// Used to simulate atmospheric friction.
	/// </summary>
	public float speedDampening,speedDampeningPrograde;
	/// <summary>
	/// Modifies velocity at which the specified speed dampening will be applied.
	/// By default, it's 1000 m/s.  If this is 0.5, then the peak speed/intended "terminal velocity" will be halved to 500 m/s.
	/// Probably best for gameplay purposes to leave this alone, except for thick atmospheres (like a gas giant's, or a Venus analogue's)
	/// </summary>
	public float dampenSpeed = 1f;
}