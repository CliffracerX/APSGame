﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AstroGuy : MonoBehaviour
{
	/// <summary>
	/// A list of all gravitational bodies on this map...
	/// </summary>
	public Transform[] gravityBodies;
	/// <summary>
	/// ...and their masses...
	/// </summary>
	public float[] masses;
	/// <summary>
	/// ...and their hoverboard strength modifiers...
	/// </summary>
	public float[] hoverMods;
	/// <summary>
	/// ...and their average radii.
	/// </summary>
	public float[] avgRadii;
	/// <summary>
	/// Astronaut Animator.
	/// </summary>
	public Animator anm;
	/// <summary>
	/// The camera pivot point.
	/// </summary>
	public Transform cameraPivot;
	/// <summary>
	/// Mouse X and Y for camera rotation.
	/// </summary>
	public float mouseX,mouseY;
	/// <summary>
	/// The astronaut's rigidbody.
	/// </summary>
	public Rigidbody myBody;
	/// <summary>
	/// The world transform, used for floating origin stuff.
	/// </summary>
	public Transform world;
	/// <summary>
	/// Used for rotation.
	/// </summary>
	public float mouseSens,turnRate;
	/// <summary>
	/// The power (in newtons (?)) of the EVA jetpack.
	/// </summary>
	public float thrustPower;
	/// <summary>
	/// Hoverboard strength per major source of influence.
	/// </summary>
	public float[] hover;
	/// <summary>
	/// EVA fuel.
	/// </summary>
	public float fuel,maxFuel;
	/// <summary>
	/// Life support.
	/// </summary>
	public float lifeSupport,maxLife;
	/// <summary>
	/// Thruster FX.
	/// </summary>
	public Transform[] thrustsSuit,thrustsBoard;
	/// <summary>
	/// Thruster lights.
	/// </summary>
	public Light[] lightsSuit,lightsBoard;
	/// <summary>
	/// Used for rescaling thruster FX.
	/// </summary>
	public Vector3[] onScalesSuit,offScalesSuit,onScalesBoard,offScalesBoard;
	/// <summary>
	/// Used for UIless UI.
	/// </summary>
	public Transform fuelBar,lifeBar;
	/// <summary>
	/// Is the astronaut relaxing?
	/// </summary>
	public bool relaxing = false;
	/// <summary>
	/// The camera transform, used for zooming in and out.
	/// </summary>
	public Transform camera;
	/// <summary>
	/// The number of stations on this map.
	/// </summary>
	public int numStations;
	/// <summary>
	/// A list of visited Station triggerboxes.  Used for teh win condition.
	/// </summary>
	public List<Collider> stationsVisited;
	/// <summary>
	/// UI canvas for doing UI things that Don't Suck (tm)
	/// </summary>
	public GameObject exitCanvas,exitCanvasDead,canvasFuel,canvasLife,canvasBoth,canvasBack,canvasSpeed;
	/// <summary>
	/// Text for the flight UI.
	/// </summary>
	public UnityEngine.UI.Text speedText,heightText;
	//yes i know putting a bunch of things on one line like this is probably a big no-no
	/// <summary>
	/// Audio source for volume changes.
	/// </summary>
	public AudioSource audioJets,audioBoard,musicGround,musicSpace,musicLowLife,musicLowFuel,musicFinalStation;
	/// <summary>
	/// GameManager instance, because calling GameManager.instance every time a reference is needed is a *pain in the arse*.
	/// </summary>
	public GameManager gm;
	/// <summary>
	/// Renderers for changing colors on.
	/// </summary>
	public Renderer playerRender, boardRender, fuelBarRender;
	/// <summary>
	/// The player point light.  Used for recolors.
	/// </summary>
	public Light playerLight;
	/// <summary>
	/// Thruster FX renderers.
	/// </summary>
	public MeshRenderer[] boardThrusterRenders,playerThrusterRenders;
	/// <summary>
	/// Your life support usage is multiplied by this number.
	/// </summary>
	public float lifeDrain;
	/// <summary>
	/// Exactly what it says on the tin.
	/// </summary>
	public float musicVolume;
	/// <summary>
	/// Reparent?  TODO: what's this used for
	/// </summary>
	public bool reParent = false;
	/// <summary>
	/// A list of Atmospheres present for atmospheric drag, life support easing, etc
	/// </summary>
	public List<Atmosphere> atmosPresent;
	/// <summary>
	/// Velocity display mode for the flight UI.
	/// Global = 0: displays the current overall velocity relative to transform.position Vector3.zero (ostensibly surface velocity if applicable added to myBody.velocity)
	/// SoI = 1: displays the current velocity relative to the current major sphere of influence (e.g, planet or moon you're orbiting/on the surface of)
	/// Surface = 2: displays the current velocity relative to the surface of the aforementioned major sphere
	/// </summary>
	public enum VelocityDisplayMode {Global=0, SoI=1, Surface=2}
	/// <summary>
	/// What velocity display mode are we using right now?
	/// </summary>
	public VelocityDisplayMode myVelocityMode;
	/// <summary>
	/// Altitude display mode for the flight UI.
	/// Altitude = 0: displays the current altitude, which is distance from object's CoM, minus object radius
	/// Distance = 1: displays the current distance from the "parent object"'s CoM
	/// </summary>
	public enum AltitudeDisplayMode {Altitude=0, Distance=1}
	/// <summary>
	/// What altitude display mode are we using right now?
	/// </summary>
	public AltitudeDisplayMode myAltitudeMode;
	/// <summary>
	/// Might be used in adding planet rotation/translation velocity to the player.
	/// TODO: does this need to exist
	/// </summary>
	public Vector3 planetVelocity;
	/// <summary>
	/// Used in adding planet rotation/translation velocity to the player.
	/// </summary>
	public Transform tempMov;
	/// <summary>
	/// A list of camera objects for setting the field of view on.
	/// </summary>
	public Camera[] cams;

	void PreUpdate()
	{
		//Debug.Log("PREUPDATE?");
		Debug.Log(tempMov == null);
		Debug.Log(transform == null);
		tempMov.transform.position = transform.position;
	}

	public void DoButton(int button)
	{
		if(button == -1)
		{
			SceneManager.LoadScene("Scenes/MenuScene");
			PreUpdateHandle.PreUpdate -= PreUpdate;
		}
		else if(button == 0) //Win!
		{
			SceneManager.LoadScene("Scenes/WinScene");
			PreUpdateHandle.PreUpdate -= PreUpdate;
		}
		else if(button == 1) //Lose!
		{
			SceneManager.LoadScene("Scenes/LoseScene");
			PreUpdateHandle.PreUpdate -= PreUpdate;
		}
		else if(button == 2) //Toggle speed display mode!
		{
			if(myVelocityMode == VelocityDisplayMode.Global)
			{
				myVelocityMode = VelocityDisplayMode.SoI;
			}
			else if(myVelocityMode == VelocityDisplayMode.SoI)
			{
				myVelocityMode = VelocityDisplayMode.Surface;
			}
			else if(myVelocityMode == VelocityDisplayMode.Surface)
			{
				myVelocityMode = VelocityDisplayMode.Global;
			}
		}
		else if(button == 3) //Toggle altitude display mode!
		{
			if(myAltitudeMode == AltitudeDisplayMode.Altitude)
			{
				myAltitudeMode = AltitudeDisplayMode.Distance;
			}
			else if(myAltitudeMode == AltitudeDisplayMode.Distance)
			{
				myAltitudeMode = AltitudeDisplayMode.Altitude;
			}
		}
	}

	void Start()
	{
		PreUpdateHandle.PreUpdate += PreUpdate;
		this.gm = GameManager.instance;
		playerRender.materials[0].color = gm.playerColorPrimary;
		playerRender.materials[1].color = gm.playerColorVisor;
		playerRender.materials[2].color = gm.playerColorTheme;
		playerRender.materials[3].color = gm.playerColorAccents;
		playerRender.materials[5].color = gm.playerColorThrust;
		playerRender.materials[6].color = gm.playerColorLight;
		float h = 0, s = 0, v = 0;
		Color.RGBToHSV(gm.playerColorLight, out h, out s, out v);
		Color tC = Color.HSVToRGB(h, s, v*0.5f);
		playerRender.materials[6].SetColor("_EmissionColor", tC);
		fuelBarRender.material.color = gm.playerColorLight;
		fuelBarRender.material.SetColor("_EmissionColor", tC);
		boardRender.materials[0].color = gm.playerColorAccents;
		boardRender.materials[1].color = gm.playerColorTheme;
		boardRender.materials[2].color = gm.playerColorThrust;
		boardRender.materials[3].color = gm.playerColorLight;
		boardRender.materials[3].SetColor("_EmissionColor", tC);
		playerLight.color = gm.playerColorLight;
		for(int i = 0; i < boardThrusterRenders.Length; i++)
		{
			boardThrusterRenders[i].material.color = gm.playerColorThrust;
			lightsBoard[i].color = gm.playerColorThrust;
		}
		for(int i = 0; i < playerThrusterRenders.Length; i++)
		{
			playerThrusterRenders[i].material.color = gm.playerColorThrust;
			lightsSuit[i].color = gm.playerColorThrust;
		}
		this.fuel = (maxFuel / 25f) * gm.pointsFuel;
		this.maxFuel = (maxFuel / 25f) * gm.pointsFuel;
		this.lifeSupport = (maxLife / 25f) * gm.pointsLife;
		this.maxLife = (maxLife / 25f) * gm.pointsLife;
		this.thrustPower = 100 * gm.pointsThruster;
		this.lifeDrain = (1 / 25f) * gm.pointsEfficiency;
		this.musicVolume = gm.musicVolume;
		foreach(Camera cam in cams)
		{
			cam.fieldOfView = gm.fov;
		}
	}

	void OnTriggerStay(Collider other)
	{
		//Debug.Log("TriggerStay");
		if(other.name.StartsWith("Station|"))
		{
			if(!stationsVisited.Contains(other))
			{
				stationsVisited.Add(other);
			}
			if(other.name.EndsWith("Life"))
			{
				this.lifeSupport = Mathf.Clamp(this.lifeSupport + ((maxLife/20)*Time.deltaTime), 0, maxLife);
				canvasLife.SetActive(true);
			}
			if(other.name.EndsWith("Fuel"))
			{
				this.fuel = Mathf.Clamp(this.fuel + ((maxFuel/10) * Time.deltaTime), 0, maxFuel);
				canvasFuel.SetActive(true);
			}
			if(other.name.EndsWith("Both"))
			{
				this.lifeSupport = Mathf.Clamp(this.lifeSupport + ((maxLife/20)*Time.deltaTime), 0, maxLife);
				this.fuel = Mathf.Clamp(this.fuel + ((maxFuel/10) * Time.deltaTime), 0, maxFuel);
				canvasBoth.SetActive(true);
			}
		}
		inTrigger = true;
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Atmos")
		{
			if(!atmosPresent.Contains(other.GetComponent<Atmosphere>()))
			{
				atmosPresent.Add(other.GetComponent<Atmosphere>());
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag == "Atmos")
		{
			if(atmosPresent.Contains(other.GetComponent<Atmosphere>()))
			{
				atmosPresent.Remove(other.GetComponent<Atmosphere>());
			}
		}
	}

	bool inTrigger = false;
	bool rayHit = false;
	RaycastHit rh;
	bool tempBoolRH,tempBoolRH1;
	Vector3 tempVelFU;
	int mI;

	void FixedUpdate()
	{
		if(GameManager.instance.enableVelocityMatching) //if custom parenting stuff is needed
		{
			if(rayHit)
			{
				bool tempBool = tempBoolRH;
				bool tempBool1 = tempBoolRH1;
				if(!tempBool)
				{
					tempMov.parent = rh.transform;
					tempBoolRH = true;
				}
				Vector3 tempVel = tempMov.transform.position - transform.position;
				tempVel *= 60;
				if(!tempBool1 && tempBool)
				{
					Debug.Log(tempVel);
					myBody.velocity -= tempVel;
					tempBoolRH = true;
					tempBoolRH1 = true;
				}
				planetVelocity = tempVel;
				transform.position += (tempVel/60) + (myBody.velocity * Time.fixedDeltaTime * Time.timeScale);
			}
			else
			{
				if(tempBoolRH)
				{
					myBody.velocity += planetVelocity;
					tempMov.parent = null;
					Debug.Log("VELOCITY REMOVE");
					tempBoolRH = false;
					tempBoolRH1 = false;
				}
				planetVelocity = gravityBodies[mI].position - tempVelFU;
			}
			tempVelFU = gravityBodies[mI].position;
		}
		if(transform.position.magnitude >= 250)
		{
			world.position -= transform.position;
			transform.position = Vector3.zero;
		}
	}

	void Update()
	{
		if(exitCanvas.activeSelf == false && stationsVisited.Count == numStations)
		{
			exitCanvas.SetActive(true);
		}
		if(exitCanvasDead.activeSelf == false && lifeSupport <= 0)
		{
			exitCanvasDead.SetActive(true);
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
		}
		if(canvasFuel.activeSelf && !inTrigger)
		{
			canvasFuel.SetActive(false);
		}
		if(canvasLife.activeSelf && !inTrigger)
		{
			canvasLife.SetActive(false);
		}
		if(canvasBoth.activeSelf && !inTrigger)
		{
			canvasBoth.SetActive(false);
		}
		canvasSpeed.SetActive(GameManager.instance.showFlightUi);
		inTrigger = false;
		float atmosMod = 1.0f;
		float atmosDrag = 0.0f;
		float dampenSpeed = 1000f;
		foreach(Atmosphere am in atmosPresent)
		{
			atmosMod *= am.lifeSupportEasing;
			atmosDrag += Mathf.Lerp(am.speedDampening, am.speedDampeningPrograde, 0.5f);
			dampenSpeed *= am.dampenSpeed;
		}
		myBody.velocity -= (myBody.velocity.normalized * atmosDrag * (myBody.velocity.magnitude / dampenSpeed)) * Time.deltaTime;
		if(lifeSupport > 0)
		{
			lifeSupport -= ((Time.deltaTime * (relaxing ? 0.1f : 1f)) / lifeDrain) * atmosMod;
			anm.SetFloat("Turn", Input.GetAxis("Horizontal"));
			anm.SetFloat("Thrust", Input.GetAxis("Vertical") * (Input.GetButton("Jump") ? 0.5f : 1.0f));
			anm.SetBool("Relaxing", relaxing);
			Quaternion q = cameraPivot.localRotation;
			q.eulerAngles = new Vector3(mouseY, mouseX, 0);
			cameraPivot.localRotation = q;
			float[] temp = new float[gravityBodies.Length];
			Vector3[] temps = new Vector3[gravityBodies.Length];
			float major = 0;
			mI = 0;
			for(int i = 0; i < gravityBodies.Length; i++)
			{
				float gravity = 0.0000000000667428f * ((myBody.mass * masses[i]) / Mathf.Pow(Vector3.Distance(transform.position, gravityBodies[i].position), 2));
				Vector3 direction = gravityBodies[i].position - transform.position;
				temps[i] = direction;
				temp[i] = (gravity / Mathf.Pow(Vector3.Distance(transform.position, gravityBodies[i].position), 2));
				direction.Normalize();
				myBody.AddForce(gravity * direction * Time.deltaTime, ForceMode.VelocityChange);
				if((gravity / Mathf.Pow(Vector3.Distance(transform.position, gravityBodies[i].position), 2)) > major)
				{
					major = (gravity / Mathf.Pow(Vector3.Distance(transform.position, gravityBodies[i].position), 2));
					mI = i;
				}
			}
			//if(!rayHit)
			{
				Vector3 tI = Vector3.zero;
				for(int i = 0; i < temps.Length; i++)
				{
					tI += temps[i] * (temp[i] / temp[mI]);
				}
				tI.Normalize();
				transform.rotation = Quaternion.FromToRotation(-transform.up, tI) * transform.rotation;
			}
			Ray r = new Ray(transform.position, -transform.up);
			rayHit = Physics.Raycast(r, out rh, 25f);
			if(!relaxing)
				transform.Rotate(0, Input.GetAxis("Horizontal") * turnRate * Time.deltaTime, 0, Space.Self);
			if(rayHit)
			{
				float hoverP = (7.5f - rh.distance) / 7.5f;
				hoverP = Mathf.Clamp(hoverP, 0, 7.5f);
				hoverP *= hoverMods[mI];
				myBody.AddForce(transform.up * (Mathf.Lerp(0, hover[mI], hoverP)) * Time.deltaTime);
				transform.rotation = Quaternion.FromToRotation(transform.up, rh.normal) * transform.rotation;
				if(Input.GetAxis("Horizontal") != 0 && !relaxing)
				{
					myBody.velocity = Vector3.RotateTowards(myBody.velocity, transform.forward, turnRate * Time.deltaTime * Mathf.Deg2Rad, 0);
				}
				for(int i = 0; i < thrustsBoard.Length; i++)
				{
					thrustsBoard[i].localScale = Vector3.Lerp(offScalesBoard[i], onScalesBoard[i], hoverP);
					lightsBoard[i].intensity = hoverP;
				}
				audioBoard.volume = Mathf.Clamp(hoverP, 0, 1);
				if(reParent)
				{
					transform.parent = rh.transform;
				}
			}
			else
			{
				for(int i = 0; i < thrustsBoard.Length; i++)
				{
					thrustsBoard[i].localScale = offScalesBoard[i];
					lightsBoard[i].intensity = 0;
				}
				audioBoard.volume = 0;
				if(reParent)
				{
					transform.parent = null;
				}
			}
			if(fuel >= 0 || rayHit)
			{
				float thrustTemp = 0;
				if(!relaxing)
				{
					myBody.AddForce(transform.forward * Input.GetAxis("Vertical") * thrustPower * Time.deltaTime);
					myBody.AddForce(transform.up * Input.GetAxis("Jump") * thrustPower * Time.deltaTime);
					thrustTemp = Mathf.Abs(Input.GetAxis("Vertical")) + Mathf.Abs(Input.GetAxis("Jump"));
					if(!rayHit)
					{
						fuel -= Time.deltaTime * (Mathf.Abs(thrustTemp));
					}
				}
				for(int i = 0; i < thrustsSuit.Length; i++)
				{
					thrustsSuit[i].localScale = Vector3.Lerp(offScalesSuit[i], onScalesSuit[i], thrustTemp / 2);
					lightsSuit[i].intensity = thrustTemp / 2;
				}
				audioJets.volume = thrustTemp / 2;
			}
			else
			{
				for(int i = 0; i < thrustsSuit.Length; i++)
				{
					thrustsSuit[i].localScale = offScalesSuit[i];
					lightsSuit[i].intensity = 0;
				}
				audioJets.volume = 0;
			}
			if(!rayHit && atmosPresent.Count==0)
			{
				musicSpace.volume = Mathf.Clamp(musicSpace.volume + (Time.deltaTime / 5), 0, musicVolume);
				musicGround.volume = Mathf.Clamp(musicGround.volume - (Time.deltaTime / 5), 0, musicVolume);
			}
			else
			{
				musicSpace.volume = Mathf.Clamp(musicSpace.volume - (Time.deltaTime / 5), 0, musicVolume);
				musicGround.volume = Mathf.Clamp(musicGround.volume + (Time.deltaTime / 5), 0, musicVolume);
			}
			if(lifeSupport <= maxLife / 5)
			{
				musicLowLife.volume = Mathf.Clamp(musicLowLife.volume + (Time.deltaTime / 5), 0, musicVolume);
			}
			else
			{
				musicLowLife.volume = Mathf.Clamp(musicLowLife.volume - (Time.deltaTime / 5), 0, musicVolume);
			}
			if(fuel <= maxFuel / 3)
			{
				musicLowFuel.volume = Mathf.Clamp(musicLowFuel.volume + (Time.deltaTime / 5), 0, musicVolume);
			}
			else
			{
				musicLowFuel.volume = Mathf.Clamp(musicLowFuel.volume - (Time.deltaTime / 5), 0, musicVolume);
			}
			if(stationsVisited.Count >= numStations - 1)
			{
				musicFinalStation.volume = Mathf.Clamp(musicFinalStation.volume + (Time.deltaTime / 5), 0, musicVolume);
			}
			else
			{
				musicFinalStation.volume = Mathf.Clamp(musicFinalStation.volume - (Time.deltaTime / 5), 0, musicVolume);
			}
			//transform.Rotate(-90, 0, 0);
			if(Input.GetButtonUp("Fire2"))
			{
				if(Cursor.visible)
				{
					Cursor.visible = false;
					Cursor.lockState = CursorLockMode.Locked;
				}
				else
				{
					Cursor.visible = true;
					Cursor.lockState = CursorLockMode.None;
				}
			}
			if(Input.GetKeyUp(KeyCode.Alpha1))
			{
				Time.timeScale = 1f;
			}
			if(Input.GetKeyUp(KeyCode.Alpha2))
			{
				Time.timeScale = 2f;
			}
			if(Input.GetKeyUp(KeyCode.Alpha3))
			{
				Time.timeScale = 4f;
			}
			if(Input.GetKeyUp(KeyCode.Alpha4))
			{
				Time.timeScale = 8f;
			}
			if(Input.GetKeyUp(KeyCode.Alpha5))
			{
				Time.timeScale = 16f;
			}
			if(Input.GetKeyUp(KeyCode.Alpha6))
			{
				Time.timeScale = 32f;
			}
			if(Input.GetKeyUp(KeyCode.Alpha7))
			{
				Time.timeScale = 64f;
			}
			if(Input.GetKeyUp(KeyCode.Alpha8))
			{
				Time.timeScale = 128f;
			}
			if(Input.GetKeyUp(KeyCode.Alpha9))
			{
				Time.timeScale = 256f;
			}
			if(Input.GetKeyUp(KeyCode.Alpha0))
			{
				Time.timeScale = 512f;
			}
			if(Input.GetButtonUp("Relax"))
			{
				relaxing = !relaxing;
			}
			if(!Cursor.visible)
			{
				mouseX = (mouseX + Input.GetAxis("Mouse X") * mouseSens * Time.deltaTime / Time.timeScale) % 360f;
				mouseY = Mathf.Clamp((mouseY - Input.GetAxis("Mouse Y") * mouseSens * Time.deltaTime / Time.timeScale), -90, 90);
				camera.localPosition = new Vector3(0, 0, camera.localPosition.z + (Input.GetAxis("Mousewheel") * Time.deltaTime * 5));
				if(canvasBack.activeSelf)
				{
					canvasBack.SetActive(false);
				}
			}
			else
			{
				if(!canvasBack.activeSelf)
				{
					canvasBack.SetActive(true);
				}
			}
			if(GameManager.instance.showFlightUi)
			{
				string localMode = "GLOBAL";
				float localSpeed = myBody.velocity.magnitude + planetVelocity.magnitude;
				if(myVelocityMode == VelocityDisplayMode.SoI)
				{
					localMode = "RELATIVE";
					localSpeed = myBody.velocity.magnitude - planetVelocity.magnitude;
				}
				else if(myVelocityMode == VelocityDisplayMode.Surface)
				{
					localMode = "SURFACE";
					localSpeed = myBody.velocity.magnitude;
				}
				speedText.text = localMode + ": " + localSpeed.ToString("000000.00") + " m/s";
				float localDist = Vector3.Distance(transform.position, gravityBodies[mI].position);
				if(myAltitudeMode == AltitudeDisplayMode.Altitude)
				{
					localDist -= avgRadii[mI];
				}
				localMode = "ALTITUDE";
				if(myAltitudeMode == AltitudeDisplayMode.Distance)
				{
					localMode = "DISTANCE";
				}
				string dType = " m";
				if(localDist >= 999999.99f) //kilometers
				{
					localDist /= 1000;
					dType = "km";
				}
				if(localDist >= 999999.99f) //gigameters
				{
					localDist /= 1000;
					dType = "gm";
				}
				if(localDist >= 999999.99f) //terameters - what person's ever gonna end up at this altitude?!
				{
					localDist /= 1000;
					dType = "tm";
				}
				heightText.text = localMode+": " + localDist.ToString("000000.00") + dType;
			}
		}
	}

	void LateUpdate() //Because the Animator decided to override the health-bar states?  I guess?
	{
		lifeBar.localScale = new Vector3(1, 1, lifeSupport / maxLife);
		fuelBar.localScale = new Vector3(1, 1, fuel / maxFuel);
	}
}