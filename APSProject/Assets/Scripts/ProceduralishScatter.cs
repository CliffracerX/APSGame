﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralishScatter : MonoBehaviour
{
	public float minheight, maxheight, genHeight;
	public GameObject prefab;
	public int numPrefabs;
	public int seed;
	public float scaleDiv;
	public LayerMask myMask;
	public QueryTriggerInteraction triggers;

	void Start()
	{
		Random.State backupState = Random.state;
		Random.InitState(seed);
		for(int i = 0; i < numPrefabs; i++)
		{
			bool found = false;
			Vector3 pos=Vector3.zero,point=Vector3.zero;
			while(!found)
			{
				Vector3 temp = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * genHeight;
				Vector3 dir = -temp.normalized;
				Ray r = new Ray(transform.position + temp, dir);
				RaycastHit rh;
				if(Physics.Raycast(r, out rh, 999999, myMask, triggers))
				{
					float dist = Vector3.Distance(transform.position, rh.point);
					//if(dist >= minheight && dist <= maxheight)
					{
						point = rh.normal;
						pos = rh.point;
						found = true;
					}
				}
			}
			GameObject instantiated = Instantiate(prefab, pos, Quaternion.LookRotation(point), this.transform);
			instantiated.transform.localScale /= scaleDiv;
		}
		Random.state = backupState;
	}
}