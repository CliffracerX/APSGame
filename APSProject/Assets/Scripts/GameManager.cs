﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class GameManager : MonoBehaviour
{
	public Color playerColorPrimary,playerColorTheme,playerColorLight,playerColorThrust,playerColorVisor,playerColorAccents;
	public int pointsLife,pointsFuel,pointsThruster,pointsEfficiency;
	public int tankMax,powerMax;
	public float musicVolume;
	public static GameManager instance;
	public Transform rotator;
	public Renderer playerRender, boardRender, fuelBarRender;
	public Light playerLight;
	public MeshRenderer[] boardThrusterRenders;
	public Light[] boardLightRenders;
	public Animator anm;
	public AudioSource music;
	enum MenuScreen {Main = 0, Options = 1, Special = 2, Levels = 3, Logs = 4}
	MenuScreen myScreen;
	public int reqAnim;
	public GameObject specialObj;
	public bool ensureBalance;
	/// <summary>
	/// A list of playable levels.
	/// </summary>
	public string[] levelNames,levelIDs;
	/// <summary>
	/// Should we show the flight user interface?
	/// </summary>
	public bool showFlightUi;
	/// <summary>
	/// Do we enable planet/moon rotation?
	/// </summary>
	public bool enableRotation;
	/// <summary>
	/// A list of update names.
	/// </summary>
	public string[] updateNames;
	[Multiline(25)]
	public string[] updateLogs;
	/// <summary>
	/// For the update log screen.
	/// </summary>
	public int selectedUpdate;
	/// <summary>
	/// The field-of-view setting.  Useful when you want that peak realism feel of being on a planet in space.
	/// </summary>
	public float fov;
	/// <summary>
	/// Lets you disable velocity matching for planets.
	/// </summary>
	public bool enableVelocityMatching;

	void Start()
	{
		GameManager.instance = this;
		if(File.Exists(Application.dataPath + "/" + "PlayerSettings.prefs"))
		{
			LoadSettings();
		}
		if(reqAnim == 0)
		{
			anm.SetBool("Relaxing", true);
		}
		else
		{
			anm.SetInteger("SpecialState", reqAnim);
			myScreen = MenuScreen.Special;
		}
	}

	public Rect winR;
	public Vector2 winScroll;

	void OnGUI()
	{
		GUI.Label(new Rect(Screen.width / 2 - 128, 16, 256, 64), "ASTRONAUT+SKATEBOARD: VER. 1.1_b1", boldLabel);
		if(myScreen == MenuScreen.Main)
		{
			//old code, for reference
			/*if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.1f, 128, 32), "PLAY", background))
			{
				myScreen = MenuScreen.Levels;
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.5f - 16, 128, 32), "OPTIONS", background))
			{
				myScreen = MenuScreen.Options;
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.9f - 32, 128, 32), "QUIT", background))
			{
				Application.Quit();
			}*/
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.1f, 128, 32), "PLAY", background))
			{
				myScreen = MenuScreen.Levels;
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.366666667f - 10.666666667f, 128, 32), "OPTIONS", background))
			{
				myScreen = MenuScreen.Options;
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.533333333f - 21.333333334f, 128, 32), "PATCH NOTES", background))
			{
				myScreen = MenuScreen.Logs;
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.9f - 32, 128, 32), "QUIT", background))
			{
				Application.Quit();
			}
		}
		else if(myScreen == MenuScreen.Options)
		{
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.1f, 128, 32), "BACK?", background))
			{
				myScreen = MenuScreen.Main;
				SaveSettings();
			}
			winR = GUI.Window(0, winR, DoWindow, "OPTIONS", window);
		}
		else if(myScreen == MenuScreen.Special)
		{
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.9f - 32, 128, 32), "BACK TO MAIN", background))
			{
				myScreen = MenuScreen.Main;
				specialObj.SetActive(false);
			}
		}
		else if(myScreen == MenuScreen.Levels)
		{
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.9f - 32, 128, 32), "BACK?", background))
			{
				myScreen = MenuScreen.Main;
			}
			for(int i = 0; i < levelIDs.Length; i++)
			{
				if(GUI.Button(new Rect(Screen.width / 2 - 64, (Screen.height * 0.1f) + (i * 32), 128, 32), levelNames[i], background))
				{
					SceneManager.LoadScene(levelIDs[i]);
				}
			}
		}
		else if(myScreen == MenuScreen.Logs)
		{
			if(GUI.Button(new Rect(Screen.width / 2 - 64, Screen.height * 0.9f - 32, 128, 32), "BACK?", background))
			{
				myScreen = MenuScreen.Main;
			}
			GUILayout.BeginArea(new Rect(Screen.width / 2 - 368, 64, 368 * 2, (Screen.height - (Screen.height * 0.1f)) - 128));
			scrollPosLogs = GUILayout.BeginScrollView(scrollPosLogs);
			for(int i = 0; i < updateNames.Length; i++)
			{
				if(i == selectedUpdate)
				{
					GUI.color = playerColorTheme;
				}
				else
				{
					GUI.color = Color.white;
				}
				if(GUILayout.Button(updateNames[i], background, GUILayout.MinHeight(32)))
				{
					selectedUpdate = i;
				}
				GUI.color = Color.white;
			}
			GUILayout.TextArea(updateLogs[selectedUpdate], GUILayout.MinHeight(768), GUILayout.MaxHeight(99999), GUILayout.ExpandHeight(true));
			GUILayout.EndScrollView();
			GUILayout.EndArea();
		}
	}

	Vector2 scrollPosLogs = Vector2.zero;

	public GUIStyle boldLabel;
	public GUIStyle label;
	public GUIStyle background;
	public GUIStyle window;

	void DoWindow(int id)
	{
		winScroll = GUILayout.BeginScrollView(winScroll);
		GUILayout.Label("-=MUSIC VOLUME=-", boldLabel);
		musicVolume = GUILayout.HorizontalSlider(musicVolume, 0f, 1f);
		GUILayout.Label("-=FIELD OF VIEW: "+fov+"=-", boldLabel);
		fov = (int)GUILayout.HorizontalSlider(fov, 15, 180);
		GUI.color = playerColorPrimary;
		GUILayout.Label("-=PRIMARY COLOR=-", boldLabel);
		GUI.color = Color.white;
		GUILayout.Label("Red/Green/Blue (0-255)", label);
		playerColorPrimary.r = float.Parse(GUILayout.TextField("" + (playerColorPrimary.r * 255), background))/255f;
		playerColorPrimary.g = float.Parse(GUILayout.TextField("" + (playerColorPrimary.g * 255), background))/255f;
		playerColorPrimary.b = float.Parse(GUILayout.TextField("" + (playerColorPrimary.b * 255), background))/255f;
		GUI.color = playerColorTheme;
		GUILayout.Label("-=THEME COLOR=-", boldLabel);
		GUI.color = Color.white;
		GUILayout.Label("Red/Green/Blue (0-255)", label);
		playerColorTheme.r = float.Parse(GUILayout.TextField("" + (playerColorTheme.r * 255), background))/255f;
		playerColorTheme.g = float.Parse(GUILayout.TextField("" + (playerColorTheme.g * 255), background))/255f;
		playerColorTheme.b = float.Parse(GUILayout.TextField("" + (playerColorTheme.b * 255), background))/255f;
		GUI.color = playerColorLight;
		GUILayout.Label("-=LIGHT COLOR=-", boldLabel);
		GUI.color = Color.white;
		GUILayout.Label("Red/Green/Blue (0-255)", label);
		playerColorLight.r = float.Parse(GUILayout.TextField("" + (playerColorLight.r * 255), background))/255f;
		playerColorLight.g = float.Parse(GUILayout.TextField("" + (playerColorLight.g * 255), background))/255f;
		playerColorLight.b = float.Parse(GUILayout.TextField("" + (playerColorLight.b * 255), background))/255f;
		GUI.color = playerColorThrust;
		GUILayout.Label("-=THRUSTER COLOR=-", boldLabel);
		GUI.color = Color.white;
		GUILayout.Label("Red/Green/Blue (0-255)", label);
		playerColorThrust.r = float.Parse(GUILayout.TextField("" + (playerColorThrust.r * 255), background))/255f;
		playerColorThrust.g = float.Parse(GUILayout.TextField("" + (playerColorThrust.g * 255), background))/255f;
		playerColorThrust.b = float.Parse(GUILayout.TextField("" + (playerColorThrust.b * 255), background))/255f;
		GUI.color = playerColorVisor;
		GUILayout.Label("-=VISOR COLOR=-", boldLabel);
		GUI.color = Color.white;
		GUILayout.Label("Red/Green/Blue (0-255)", label);
		playerColorVisor.r = float.Parse(GUILayout.TextField("" + (playerColorVisor.r * 255), background))/255f;
		playerColorVisor.g = float.Parse(GUILayout.TextField("" + (playerColorVisor.g * 255), background))/255f;
		playerColorVisor.b = float.Parse(GUILayout.TextField("" + (playerColorVisor.b * 255), background))/255f;
		GUI.color = playerColorAccents;
		GUILayout.Label("-=ACCENTS COLOR=-", boldLabel);
		GUI.color = Color.white;
		GUILayout.Label("Red/Green/Blue (0-255)", label);
		playerColorAccents.r = float.Parse(GUILayout.TextField("" + (playerColorAccents.r * 255), background))/255f;
		playerColorAccents.g = float.Parse(GUILayout.TextField("" + (playerColorAccents.g * 255), background))/255f;
		playerColorAccents.b = float.Parse(GUILayout.TextField("" + (playerColorAccents.b * 255), background))/255f;
		GUILayout.Label("-=TANK DISTRIBUTION=-", boldLabel);
		ensureBalance = GUILayout.Toggle(ensureBalance, "Ensure balanced stats?");
		GUILayout.Label("Fuel Tanks: "+(pointsFuel*(60f/25f))+"s of fuel", label);
		pointsFuel = int.Parse(GUILayout.TextField("" + pointsFuel, background));
		GUILayout.Label("Life Support: will last "+(pointsLife*(600f/25f))+"s standing", label);
		pointsLife = int.Parse(GUILayout.TextField("" + pointsLife, background));
		if(ensureBalance)
		{
			if(pointsFuel + pointsLife > tankMax)
			{
				pointsFuel = tankMax - pointsLife;
			}
			if(pointsFuel <= 0)
			{
				pointsFuel = 1;
				pointsLife = tankMax - 1;
			}
			if(pointsLife <= 0)
			{
				pointsFuel = tankMax - 1;
				pointsLife = 1;
			}
		}
		GUILayout.Label("-=POWER EFFICIENCY=-", boldLabel);
		GUILayout.Label("Thrusters: "+(pointsThruster*0.1f)+"kN of thrust", label);
		pointsThruster = int.Parse(GUILayout.TextField("" + pointsThruster, background));
		GUILayout.Label("Life Support: "+((1f/(pointsEfficiency*0.04f))*100f)+"% usage", label);
		pointsEfficiency = int.Parse(GUILayout.TextField("" + pointsEfficiency, background));
		if(ensureBalance)
		{
			if(pointsThruster + pointsEfficiency > tankMax)
			{
				pointsThruster = tankMax - pointsEfficiency;
			}
			if(pointsThruster <= 0)
			{
				pointsThruster = 1;
				pointsEfficiency = tankMax - 1;
			}
			if(pointsEfficiency <= 0)
			{
				pointsThruster = tankMax - 1;
				pointsEfficiency = 1;
			}
		}
		GUILayout.Label("-=OTHER OPTIONS=-", boldLabel);
		if(GUILayout.Button("FLIGHT UI: " + (showFlightUi ? "ON" : "OFF"), background))
		{
			showFlightUi = !showFlightUi;
		}
		GUILayout.Label("The Flight UI enables two meters at the top of your screen.  One measures current velocity, in GLOBAL, SPHERE OF INFLUENCE, and SURFACE modes.\n" +
			"In GLOBAL mode, it displays your global velocity, relative to the Unity world itself - i.e, the position Vector3.zero.\n" +
			"In SPHERE OF INFLUENCE mode, it displays your velocity relative to whatever your most major influencer is.  E.G, relative to whatever you're orbiting.\n" +
			"SOI mode is useful when you're working out velocity for something orbiting a different something, like a moon.\n" +
			"In SURFACE mode, it displays your velocity relative to the point on the surface of your major influencer that'd be made connecting a line from you to its CoM.\n" +
			"Surface mode is useful when you're trying to make a smooth landing, or perhaps establish geosynchronous orbit.\n" +
			"\n" +
			"The ALTITUDE meter measures your altitude in regards to whatever your most major influencer is.\n" +
			"It can be toggled between DISTANCE and ALTITUDE.\n" +
			"In DISTANCE mode, it's just the distance between your CoM, and its CoM.\n" +
			"In ALTITUDE mode, it's the same distance, but with the average surface radius subtracted from it.\n" +
			"\n" +
			"Most players can and probably should leave the flight UI off.  If you're a skilled Kerbonaut, or otherwise have space experience - turn it on!\n" +
			"Every space concept you're familiar with from, say, Kerbal Space Program should hold true here.  Just keep in mind - it uses UNIVERSAL GRAVITATION.\n" +
			"It's based on N-Body simulations, and as such, is a bit less forgiving than KSP when it comes to, say, maintaining orbits.\n" +
			"You can also quite possibly slide off a moon if you're not careful.", label);
		if(GUILayout.Button("ROTATION (VERY WIP): " + (enableRotation ? "ON" : "OFF"), background))
		{
			enableRotation = !enableRotation;
		}
		GUILayout.Label("This option enables or disables PLANETARY ROTATION.  As well as moon rotation.  These make a MUCH MORE REALISTIC EXPERIENCE.\n" +
			"Not to mention, watching a planet rotate on its own axis while you orbit it, either from its moon, or in a free orbit of your own?  No experience like it.\n" +
			"However, this is a VERY WORK IN PROGRESS FEATURE not recommended for a normal game.  If you want to just reach all the stations and win?  Leave it off.\n" +
			"If you want the amazing experience of a more realistic KSP-ish planetary system?  Turn it on for a bit, but be ready for glitches galore.\n" +
			"Check back here frequently to find out if it's working yet!", label);
		if(GUILayout.Button("VELOCITY MATCHING: " + (enableVelocityMatching ? "ON" : "OFF"), background))
		{
			enableVelocityMatching = !enableVelocityMatching;
		}
		GUILayout.Label("This option is meant to be used alongside the rotation option; it enables and disables velocity matching to a rotating body.\n\n" +
			"In most cases, it's good to have this on, but slow-moving massive bodies, like Earth, can cause...glitchiness, to say the least.", label);
		GUILayout.EndScrollView();
		GUI.DragWindow();
	}

	void Update()
	{
		music.volume = musicVolume;
		rotator.Rotate(0, Time.deltaTime * 12f, 0);
		playerRender.materials[0].color = playerColorPrimary;
		playerRender.materials[1].color = playerColorVisor;
		playerRender.materials[2].color = playerColorTheme;
		playerRender.materials[3].color = playerColorAccents;
		playerRender.materials[5].color = playerColorThrust;
		playerRender.materials[6].color = playerColorLight;
		float h = 0, s = 0, v = 0;
		Color.RGBToHSV(playerColorLight, out h, out s, out v);
		Color tC = Color.HSVToRGB(h, s, v*0.5f);
		playerRender.materials[6].SetColor("_EmissionColor", tC);
		fuelBarRender.material.color = playerColorLight;
		fuelBarRender.material.SetColor("_EmissionColor", tC);
		boardRender.materials[0].color = playerColorAccents;
		boardRender.materials[1].color = playerColorTheme;
		boardRender.materials[2].color = playerColorThrust;
		boardRender.materials[3].color = playerColorLight;
		boardRender.materials[3].SetColor("_EmissionColor", tC);
		playerLight.color = playerColorLight;
		for(int i = 0; i < boardThrusterRenders.Length; i++)
		{
			boardThrusterRenders[i].material.color = playerColorThrust;
			boardLightRenders[i].color = playerColorThrust;
		}
	}

	void LoadSettings()
	{
		StreamReader sr = new StreamReader(Application.dataPath + "/" + "PlayerSettings.prefs");
		//Primary color first
		playerColorPrimary = new Color(float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()));
		//Then theme color (the green armbands and stuff)
		playerColorTheme = new Color(float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()));
		//Then light color (the light on your EVA jetpack, as well as the color of your fuel bar, and the glowy footrests on the board)
		playerColorLight = new Color(float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()));
		//Then thruster color (used both for lighting and actual thrust effects
		playerColorThrust = new Color(float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()));
		//Then visor color (the visor on your helmet)
		playerColorVisor= new Color(float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()));
		//Then finally the accent color, used for your gloves, boots, board, and headlamps.
		playerColorAccents = new Color(float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()), float.Parse(sr.ReadLine()));
		//Then the number of points invested in life support storage.
		pointsLife = int.Parse(sr.ReadLine());
		//Then the number of points invested in fuel storage.
		pointsFuel = int.Parse(sr.ReadLine());
		//Then the number of points invested in thruster power.
		pointsThruster = int.Parse(sr.ReadLine());
		//Then the number of points invested in life support efficiency.
		pointsEfficiency = int.Parse(sr.ReadLine());
		//Next, the requested music volume.
		musicVolume = float.Parse(sr.ReadLine());
		//Finally, do we enforce "balance" or not?  (With this off, you can put in any values you want for tank size, thrust power, etc.)
		ensureBalance = bool.Parse(sr.ReadLine());
		if(!sr.EndOfStream) //Accounting for 1.0 clients so they won't blow up
		{
			//Do we show the flight UI?  This adds a velocity meter and altitude counter, good for spacenerds like me who wanna do orbits & such.
			showFlightUi = bool.Parse(sr.ReadLine());
			//Do planets rotate & their satellites orbit?
			enableRotation = bool.Parse(sr.ReadLine());
			//What is the field of view the player has selected?
			fov = float.Parse(sr.ReadLine());
			//Do we handle surface velocity matching?
			enableVelocityMatching = bool.Parse(sr.ReadLine());
		}
		sr.Close();
	}

	void SaveSettings()
	{
		StreamWriter sw = new StreamWriter(Application.dataPath + "/" + "PlayerSettings.prefs");
		//Primary color
		sw.WriteLine(playerColorPrimary.r);
		sw.WriteLine(playerColorPrimary.g);
		sw.WriteLine(playerColorPrimary.b);
		//Theme color
		sw.WriteLine(playerColorTheme.r);
		sw.WriteLine(playerColorTheme.g);
		sw.WriteLine(playerColorTheme.b);
		//Light color
		sw.WriteLine(playerColorLight.r);
		sw.WriteLine(playerColorLight.g);
		sw.WriteLine(playerColorLight.b);
		//Thrust color
		sw.WriteLine(playerColorThrust.r);
		sw.WriteLine(playerColorThrust.g);
		sw.WriteLine(playerColorThrust.b);
		//Visor color
		sw.WriteLine(playerColorVisor.r);
		sw.WriteLine(playerColorVisor.g);
		sw.WriteLine(playerColorVisor.b);
		//Accents color
		sw.WriteLine(playerColorAccents.r);
		sw.WriteLine(playerColorAccents.g);
		sw.WriteLine(playerColorAccents.b);
		//Points in life support tank
		sw.WriteLine(pointsLife);
		//Points in EVA fuel tank
		sw.WriteLine(pointsFuel);
		//Points in thruster power
		sw.WriteLine(pointsThruster);
		//Points in life support efficiency
		sw.WriteLine(pointsEfficiency);
		//Music volume!
		sw.WriteLine(musicVolume);
		//Ensure balance?
		sw.WriteLine(ensureBalance);
		//Do we show flight UI (speed, altitude)
		sw.WriteLine(showFlightUi);
		//Do planets rotate and moons/stations orbit them?
		sw.WriteLine(enableRotation);
		//What is the field of view the player has selected?
		sw.WriteLine(fov);
		//Do we handle surface velocity matching?
		sw.WriteLine(enableVelocityMatching);
		sw.Close();
	}
}