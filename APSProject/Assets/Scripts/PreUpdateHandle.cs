﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// PreUpdate handler.
/// Idea courtesy of https://answers.unity.com/questions/614343/how-to-implement-preupdate-function.html
/// 
/// This is meant for running a function before LITERALLY ANYTHING ELSE.  Use with caution and wisdom.
/// </summary>
public class PreUpdateHandle : MonoBehaviour
{
	public static event System.Action PreUpdate;

	void Update()
	{
		if(PreUpdate != null)
		{
			PreUpdate();
		}
	}
}